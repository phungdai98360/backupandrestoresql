﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackupAndRestore
{
    static class Program
    {
        public static string tenDatabaseCreateDevice = "";
        public static string tenDuongDanCreateDevice = "E:\\backup_restore_DB\\";
        public static string tenDevice = "";
        public static string tenDeviceFull = "";
        public static string deviceWithInit = "";
        public static Boolean statusDevice = false;
        public static Boolean statusRestoreTime = false;
        public static int positionMax = 0;
        public static string timeRestore = "";
        //DN
        public static SqlConnection conn = new SqlConnection();
        public static string mLogin = "";
        public static string passWord = "";
        public static string connstr = "";
        public static int KetNoi()
        {
            if (Program.conn != null && Program.conn.State == ConnectionState.Open)
                Program.conn.Close();
            try
            {
                Program.connstr = "Data Source=DESKTOP-L2B0UP7; User ID="+Program.mLogin+";password="+passWord;
                Program.conn.ConnectionString = Program.connstr;
                Program.conn.Open();
                return 1;
            }

            catch (Exception e)
            {
                MessageBox.Show("Lỗi kết nối cơ sở dữ liệu.\nBạn xem lại user name và password.\n " + e.Message, "", MessageBoxButtons.OK);
                return 0;
            }
        }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Login());
        }
    }
}
