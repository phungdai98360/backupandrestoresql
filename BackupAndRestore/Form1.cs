﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackupAndRestore
{
    public partial class Form1 : Form
    {
        Controller controller;
        public Form1()
        {
            InitializeComponent();
            controller = new Controller();

        }
        public void ShowALLDataBase()
        {
            DataTable dt = controller.getAllDatabase();
            dataGridView1.DataSource = dt;
        }
        public void ShowALLDevice(string database)
        {
            DataTable dt = controller.getAllDevice(database);
            dataGridView2.DataSource = dt;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            ShowALLDataBase();
            //ShowALLDevice("CHUNGKHOAN");
            string database = dataGridView1.Rows[0].Cells["CSDL"].Value.ToString();
            //string position = dataGridView2.Rows[0].Cells["position"].Value.ToString();
            Program.tenDatabaseCreateDevice = database;
            Program.tenDeviceFull = "DIVECE_" + database;
            Program.tenDatabaseCreateDevice = database;
            //textBox1.Text = dataGridView2.Rows[0].Cells["position"].Value.ToString();
            //MessageBox.Show(Program.tenDeviceFull);
            dateTimePicker2.Format = DateTimePickerFormat.Custom;
            dateTimePicker2.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            if (controller.checkDatabaseDevice(database)<=0)
            {
                createDevice.Enabled = true;
                backup.Enabled = false;
            }    
            else
            {
                createDevice.Enabled = false;
                backup.Enabled = true;
            }    
            ShowALLDevice(database);
            textBox1.Text = dataGridView2.Rows[0].Cells["position"].Value == null ? "0" : dataGridView2.Rows[0].Cells["position"].Value.ToString();
            Program.positionMax= dataGridView2.Rows[0].Cells["position"].Value == null ? 0 :Int32.Parse(dataGridView2.Rows[0].Cells["position"].Value.ToString());
            Program.timeRestore = dataGridView2.Rows[0].Cells["backup_finish_date"].Value.ToString();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            if (index >= 0)
            {
                string database = dataGridView1.Rows[index].Cells["CSDL"].Value.ToString();
                Program.tenDatabaseCreateDevice = database;
                Program.tenDeviceFull = "DIVECE_" + database;
                //MessageBox.Show(Program.tenDeviceFull);
                if(controller.checkDatabaseDevice(Program.tenDatabaseCreateDevice)<=0)
                {
                    createDevice.Enabled = true;
                    backup.Enabled = false;
                }
                else
                {
                    createDevice.Enabled = false;
                    backup.Enabled = true;
                }
                ShowALLDevice(database);
                textBox1.Text = dataGridView2.Rows[0].Cells["position"].Value==null?"0": dataGridView2.Rows[0].Cells["position"].Value.ToString();
                Program.positionMax = dataGridView2.Rows[0].Cells["position"].Value == null ? 0 : Int32.Parse(dataGridView2.Rows[0].Cells["position"].Value.ToString());
            }
        }

        private void createDevice_Click(object sender, EventArgs e)
        {
            string tenDevice = "DIVECE_" + Program.tenDatabaseCreateDevice;
            Program.tenDeviceFull = tenDevice;
            string tenDuongDan = Program.tenDuongDanCreateDevice + "" + tenDevice + ".bak";
            if(controller.createDevice(tenDevice,tenDuongDan))
            {
                MessageBox.Show("Tạo thành công");
                createDevice.Enabled = false;
                backup.Enabled = true;
            } 
            else
            {
                MessageBox.Show("Đã có lỗi");
            }    
         
            //string sql = "EXEC sp_addumpdevice 'disk','"+tenDevice+"','"+tenDuongDan+"'";
            
        }

        private void backup_Click(object sender, EventArgs e)
        {
            if(Program.deviceWithInit == "WITH INIT")
            {
                if (MessageBox.Show("Hành động này sẽ xóa các bản sao lưu cũ và tạo bản sao lưu mới bạn có chắc chắn thực hiện không", "Thong bao", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    if (controller.saoLuu(Program.tenDatabaseCreateDevice, Program.tenDeviceFull, Program.deviceWithInit))
                    {

                        MessageBox.Show("Tạo thành công");
                        ShowALLDevice(Program.tenDatabaseCreateDevice);
                    }
                    else
                    {
                        MessageBox.Show("Đã có lỗi");
                    }
                }    
            } 
            else
            {

                if (controller.saoLuu(Program.tenDatabaseCreateDevice, Program.tenDeviceFull, Program.deviceWithInit))
                {

                    MessageBox.Show("Tạo thành công");
                    ShowALLDevice(Program.tenDatabaseCreateDevice);
                }
                else
                {
                    MessageBox.Show("Đã có lỗi");
                }
            }
            //Program.deviceWithInit = "WITH INIT";
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox1.Checked)
            {
                Program.deviceWithInit = "WITH INIT";
            }
            else
            {
                Program.deviceWithInit = "";
            }    
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            if(index>=0)
            {
                textBox1.Text= dataGridView2.Rows[index].Cells["position"].Value.ToString();
                Program.timeRestore = dataGridView2.Rows[index].Cells["backup_finish_date"].Value.ToString();
                //MessageBox.Show(Program.timeRestore);
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                Program.statusRestoreTime=true;
                //MessageBox.Show("" + Program.statusRestoreTime);
            }
            else
            {
                Program.statusRestoreTime = false;
                //MessageBox.Show("" + Program.statusRestoreTime);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(Program.statusRestoreTime==false)
            {
                int position = Int32.Parse(textBox1.Text);
                if (Program.statusRestoreTime == false)
                {
                    if (controller.RestoreBackuped(position, Program.tenDatabaseCreateDevice, Program.tenDeviceFull))
                    {
                        MessageBox.Show("Phục hồi thành công");
                    }
                    else
                    {
                        MessageBox.Show("Phục hồi thất bại");
                    }
                }
            }
            else
            {
               
                string time = dateTimePicker2.Text;
                DateTime ThoiDiemPhucHoi = DateTime.Parse(time);
                DateTime ThoiDiemBK = DateTime.Parse(Program.timeRestore);
                if(ThoiDiemPhucHoi<ThoiDiemBK)
                {
                    MessageBox.Show("Ngày giờ bạn chọn phải sau thời gian bản sao lưu bạn chọn và trước thời điểm cần phục hồi tồi tiếu 3 p \n"+"Vui lòng nhập lại");
                }
                else
                {
                    //MessageBox.Show("ok");
                    int position = Int32.Parse(textBox1.Text);
                    if (controller.RestoreTime(Program.tenDatabaseCreateDevice, Program.tenDeviceFull, position, time))
                    {
                        MessageBox.Show("Phục hồi thành công");
                    }
                    else
                    {
                        MessageBox.Show("Phục hồi thất bại");
                    }
                }
            }
        }
    }
}
