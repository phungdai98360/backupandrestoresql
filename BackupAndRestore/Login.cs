﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackupAndRestore
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtMlogin.Text.Trim() == "" || txtPassword.Text.Trim() == "")
            {
                MessageBox.Show("Login name và mật mã không được trống", "", MessageBoxButtons.OK);
                return;
            }
            else
            {
                Program.mLogin = txtMlogin.Text;
                Program.passWord = txtPassword.Text;
                if (Program.KetNoi() == 0) MessageBox.Show("Đăng nhập thất bại");
                else {
                    MessageBox.Show("Đăng nhập thành công");
                    Form1 fm = new Form1();
                    fm.Show();
                }
            }
        }
    }
}
