﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackupAndRestore
{
    class Controller
    {
        DataConnection dc;
        SqlDataAdapter da;
        SqlCommand cmd;
        public Controller()
        {
            dc = new DataConnection();
        }
        public DataTable getAllDatabase()
        {
            string sql = "SELECT 'Cơ sở dữ liệu'=name FROM sys.databases " +
            "WHERE(database_id >= 5) AND(NOT(name LIKE N'ReportServer%')) "+
            "ORDER BY name";
            //B2:Tạo 1 connect đến sql
            SqlConnection con = dc.getConnect();
            //B3 :Khoi tao lop SqlDataAdapter
            da = new SqlDataAdapter(sql, con);
            //B4:
            con.Open();
            //B5 Đổ dl ra datatable
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public DataTable getAllDevice(string database)
        {
            string sql = "SELECT position,dd='',backup_finish_date,user_name " +
                            "FROM MSDB.DBO.backupset "+
                            "WHERE database_name ='"+database+"' "+ "AND type = 'D' AND "+
                            "backup_set_id >= "+
                            "( "+
                                "SELECT MAX(backup_set_id) FROM MSDB.DBO.backupset " +
                                "WHERE media_set_id = "+
                                "( "+
                                "SELECT MAX(media_set_id) "+

                                "FROM MSDB.DBO.backupset "+
                                "WHERE database_name = '"+database+"' "+ "AND type = 'D' "+
	                            ") "+
	                            "AND position = 1 "+
                            ") "+
                            "ORDER BY position DESC";
            //B2:Tạo 1 connect đến sql
            SqlConnection con = dc.getConnect();
            //B3 :Khoi tao lop SqlDataAdapter
            da = new SqlDataAdapter(sql, con);
            //B4:
            con.Open();
            //B5 Đổ dl ra datatable
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public int checkDatabaseDevice(string database)
        {
            string sql = "SELECT position,dd='',backup_finish_date,user_name " +
                            "FROM MSDB.DBO.backupset " +
                            "WHERE database_name ='" + database + "' " + "AND type = 'D' AND " +
                            "backup_set_id >= " +
                            "( " +
                                "SELECT MAX(backup_set_id) FROM MSDB.DBO.backupset " +
                                "WHERE media_set_id = " +
                                "( " +
                                "SELECT MAX(media_set_id) " +

                                "FROM MSDB.DBO.backupset " +
                                "WHERE database_name = '" + database + "' " + "AND type = 'D' " +
                                ") " +
                                "AND position = 1 " +
                            ") " +
                            "ORDER BY position DESC";
            //B2:Tạo 1 connect đến sql
            SqlConnection con = dc.getConnect();
            //B3 :Khoi tao lop SqlDataAdapter
            da = new SqlDataAdapter(sql, con);
            //B4:
            con.Open();
            //B5 Đổ dl ra datatable
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt.Rows.Count;
        }
        public Boolean createDevice(string tenDevice, string tenDuongDan)
        {
            Boolean status = false;
            string sql = "EXEC sp_addumpdevice 'disk','" + tenDevice + "','" + tenDuongDan + "'";
            try
            {
                SqlConnection con = dc.getConnect();
                SqlCommand cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                status = true;
            }
            catch (Exception)
            {

                status = false;
            }
            return status;
        }
        public Boolean saoLuu(string database,string device,string withInit)
        {
            Boolean status = false;
            string sql= "BACKUP DATABASE "+database+ " TO "+ device+" "+withInit;
            //MessageBox.Show(sql);
            try
            {
                SqlConnection con = dc.getConnect();
                SqlCommand cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                status = true;
            }
            catch (Exception)
            {

                status = false;
            }
            return status;
        }
        public Boolean RestoreBackuped(int positiion,string database,string device)
        {
            Boolean status = false;
            string sql= "ALTER DATABASE "+database+" SET SINGLE_USER WITH ROLLBACK IMMEDIATE "+
                        "USE tempdb "+
                        "RESTORE DATABASE "+database+" FROM " + device+" WITH FILE =" +positiion+", REPLACE "+
                        "ALTER DATABASE " + database + "  SET MULTI_USER";
            try
            {
                SqlConnection con = dc.getConnect();
                SqlCommand cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                status = true;
            }
            catch (Exception)
            {

                status = false;
            }
            return status;
        }
        public Boolean RestoreTime(string database, string device,int position,string time)
        {
            Boolean status = false;
            string sql = "ALTER DATABASE "+database+" SET SINGLE_USER WITH ROLLBACK IMMEDIATE;\n"+ 
                "BACKUP LOG "+database+" TO DISK = 'E:\\backup_restore_DB\\"+database+ "_LOG.trn' WITH INIT,NORECOVERY;\n" +
                "USE tempdb \n"+
                "RESTORE DATABASE "+database+" FROM "+device+" WITH FILE="+position+", NORECOVERY ;\n"+
                "RESTORE DATABASE "+database+" FROM DISK = 'E:\\backup_restore_DB\\"+database+"_LOG.trn' WITH STOPAT = '"+time+"' \n"+
                "ALTER DATABASE "+database+" SET MULTI_USER";
            MessageBox.Show(sql);
            try
            {
                SqlConnection con = dc.getConnect();
                SqlCommand cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                status = true;
            }
            catch (Exception)
            {

                status = false;
            }
            return status;
        }
    }
    
}
